using BlogApi3;
using DDDInfrastructure.Contracts;
using DomainApplication.Entities;
using Moq;
using NUnit.Framework;

namespace BlogApi3TestProject
{
    public class ServiceUnitTests
    {
        private Mock<IRepository> _repository;
        private Service _service;
        private Post _post;
        private PostDTO _postDTO;
        private readonly int POST_ID = 1;

        [SetUp]
        public void Setup()
        {
            _repository = new Mock<IRepository>();
            _post = new Post();
            _postDTO = new PostDTO();
        }

        [Test]
        public void Service_ObtenerPostPorId_DebeRetornarDTO()
        {
            _post.Content = "content setted from unit test code";
            _repository.Setup(x => x.GetPostById(POST_ID)).Returns(_post);
            _service = new Service(_repository.Object);
            var result = _service.ObtenerPostPorId(POST_ID);
            Assert.IsInstanceOf<PostDTO>(result);
            //Assert.Equals(result.Content, "content setted from unit test code");
        }
    }
}